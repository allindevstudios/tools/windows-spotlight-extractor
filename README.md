# Windows 10 Spotlight Extractor

This tool extracts the lockscreens downloaded by Windows Spotlight to the folder you select in the software, defaulting to C:/Users/YOU/LockScreenBackgrounds. It separates the found backgrounds in Desktop and Phone images. You can then set the Desktop folder to be used as a desktop wallpaper slideshow!

Currently only works on 64-bit systems. If you want to build this yourself for 32-bit systems, great! Please install [pyInstaller](https://pyinstaller.readthedocs.io/en/stable/installation.html) and run the following command:

> pyinstaller -F -w spotlight_extractor.py

If you were successful, don't hesitate to contact me to see whether we can add it as a release.

### Source files

The extraction code is found in lock_screen_exporter.py. spotlight_extractor.py is the quick and dirty Tkinter GUI build around this extraction. vanilla.py is my original implementation, and can be run as well. This will probably be used for the GUI-less build.

### Future

While I currently consider the code in this project feature-complete, there's a lot of polish that is lacking still. Things I am considering making are:

- A build script that detects whether your system is 32-bit or 64-bit and builds a correctly named executable (example: spotlight_extractor_x86.exe)
- Rework the GUI to be better, and make a GUI-less executable that just does the default export.
- Look into executable signing.