from pathlib import Path
import os
from PIL import Image as im
import shutil

base_path = str(Path.home())

desktop_folder = 'Desktop/'

phone_folder = 'Phone/'

# 50 kB should be okay to filter out all icons and thumbnails.
minimum_size_kb = 50


minimum_size = minimum_size_kb * (2 ** 10)

source_folder = '%s/AppData/Local/Packages/Microsoft.Windows.ContentDeliveryManager_cw5n1h2txyewy/LocalState/Assets/' % base_path

backgrounds_folder = '%s/LockscreenBackgrounds/' % base_path


if not os.path.isdir(backgrounds_folder):
    os.mkdir(backgrounds_folder)

if not os.path.isdir(backgrounds_folder + desktop_folder):
    os.mkdir(backgrounds_folder + desktop_folder)

if not os.path.isdir(backgrounds_folder + phone_folder):
    os.mkdir(backgrounds_folder + phone_folder)

for filename in os.listdir(source_folder):
    if os.path.getsize(source_folder + filename) > minimum_size:
        shutil.copyfile(source_folder + filename, backgrounds_folder + filename + '.png')

for filename in os.listdir(backgrounds_folder):
    if filename == desktop_folder[:-1] or filename == phone_folder[:-1]:
        continue
    with im.open(backgrounds_folder + filename) as image:
        width, height = image.size
        if width == 1920:
            shutil.copyfile(backgrounds_folder + filename,
                            backgrounds_folder + desktop_folder + filename)
        elif width == 1080:
            shutil.copyfile(backgrounds_folder + filename,
                            backgrounds_folder + phone_folder + filename)
    os.remove(backgrounds_folder + filename)


os.system('start %s' % backgrounds_folder)