from tkinter import *
from tkinter import ttk
from tkinter import filedialog
from lock_screen_exporter import LockScreenExporter

from pathlib import Path
from os.path import join

default_path = join(str(Path.home()), 'LockScreenBackgrounds')

def export(*args):
    folder = destination_entry.get()
    lse = LockScreenExporter(folder)
    lse.run_export_routine()

def get_directory(*args):
    foldername = filedialog.askdirectory(initialdir='.')
    if foldername:
        destination_folder.set(foldername)

root = Tk()
root.title("Lock Screen Exporter")

mainframe = ttk.Frame(root, padding="3 3 3 3")
mainframe.grid(column=0, row=0, sticky=(N, W, E, S))
mainframe.columnconfigure(0, weight=1)
mainframe.rowconfigure(0, weight=1)

destination_folder = StringVar()
destination_folder.set(default_path)

destination_entry = ttk.Entry(mainframe, width=30, textvariable=destination_folder)
destination_entry.grid(column=0, row=0, sticky=(W, E))

ttk.Button(mainframe, text="SELECT", command=get_directory).grid(column=1, row=0, sticky=(W, E), padx=3)
ttk.Button(mainframe, text="EXPORT", command=export, default="active").grid(column=0, row=1, sticky=(W, E), columnspan=2, pady=2)

# for child in mainframe.winfo_children(): child.grid_configure(padx=5, pady=5)

root.bind('<Return>', export)

root.mainloop()