from os import listdir, mkdir, system
from os.path import join, isdir, getsize
from shutil import copyfile
from pathlib import Path

from PIL import Image as im

class LockScreenExporter:
    base_path = str(Path.home())
    minimum_size = 50 * (2 ** 10)

    def __init__(self, destination_folder=None):
        self.source_folder = join(self.base_path,
            'AppData\\Local\\Packages\\Microsoft.Windows.ContentDeliveryManager_cw5n1h2txyewy\\LocalState\\Assets\\')

        self.destination_folder = destination_folder
        if destination_folder is None:
            self.destination_folder = join(self.base_path, 'LockScreenBackgrounds')

        self.desktop_destination_folder = join(self.destination_folder, 'Desktop')
        self.phone_destination_folder = join(self.destination_folder, 'Phone')

    def make_destination_folders(self):
        for folder in [self.destination_folder, self.desktop_destination_folder, self.phone_destination_folder]:
            if not isdir(folder):
                mkdir(folder)

    def is_background_image(self, filename):
        if getsize(filename) > self.minimum_size:
            return True
        return False
        
    def export_files(self):
        for filename in listdir(self.source_folder):
            filepath = join(self.source_folder, filename)
            if self.is_background_image(filepath):
                with im.open(filepath) as image:
                    width, height = image.size
                    if width == 1920:
                        export_filepath = join(self.desktop_destination_folder, filename)
                    elif width == 1080:
                        export_filepath = join(self.phone_destination_folder, filename)
                    else:
                        continue
                    copyfile(filepath, export_filepath + '.png')

    def open_destination_folder(self):
        system('start %s' % self.destination_folder)

    def run_export_routine(self):
        self.make_destination_folders()
        self.export_files()
        self.open_destination_folder()


def main():
    lse = LockScreenExporter()
    lse.run_export_routine()

if __name__ == '__main__':
    main()